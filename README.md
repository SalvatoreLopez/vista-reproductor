# Reproductor de Musica

En este repositorio se incluye el código, de un reproductor de música.


![Portada del proyecto](assets/cover.png)

# Versión

* 0.5

# Lenguajes usados

* php
* mysql
* html
* css
* javascript 

# Cosas agregadas 

* corrección de error para guardar estado de audio activado o desactivado
* contar canciones totales 
* modificación de cover en widget 
