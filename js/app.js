let ip = "127.0.0.1";
let url = `http://localhost/reproductor-Spoty5/`;




fetch(url)
	.then((res) => res.json())
	.then((songs) => {

		let tiempoActual = document.getElementById("tiempoActual");
		let tiempoTotal = document.getElementById("tiempoTotal");
		let totalCanciones = document.getElementById("totalCanciones");
		let botonReproducir = document.getElementById("botonReproducir");
		let muted = document.getElementById("muted");
		
		let play = document.getElementById("play");
		let prev = document.getElementById("prev");
		let next = document.getElementById("next");
		let cover = document.getElementById("cover");
		let coverWidget = document.getElementById("coverWidget");
		let lista_canciones = document.getElementById("lista_canciones");
		let nombre_cancion = document.getElementById("nombre_cancion");
		let artista = document.getElementById("artista");
		var current_track = localStorage.getItem("ultimaCancion") != null ? localStorage.getItem("ultimaCancion") : 0;
		var song, audio, duration, currentTime;
		var playing = false;
		
		

		function songActive(indice) {
			lista_canciones.innerHTML = "";
			
			songs.forEach((song, index) => {
				lista_canciones.innerHTML += `<li class="list-group-item bg-dark-2 text-white rounded-0 ${index == indice ? "song-active" : ""}" data-cancion-activa = "${index}" ><div class="media"><i class="fas fa-music ml-3 mr-3 mt-3 text-muted"></i><div class="media-body row"><div class="col-xl-8"><p class="mb-0"> ${song.nombre}</p>  <p class="mb-0 text-muted"> ${song.artista}</p></div><div class="col-xl-4"><p class="mb-0 mt-3 " id="tiempoCancion${index}">
					${
					(() => {
						let tempSong = new Audio();
						tempSong.src = `${url}${song.srcSong}`;
						tempSong.addEventListener("loadedmetadata", function () {
							let tiempoCancion = document.getElementById("tiempoCancion" + index);
							tiempoCancion.innerHTML = milisegundoSegundoMinutos(this.duration);
						}, false);
						return "";
					})()
					}
					</p></div></div></div></li>`;
			});
 
			totalCanciones.innerHTML = songs.length+" canciones";

			$(".list-group .list-group-item").click(function (e) {
				current_track = $(this).data("cancionActiva");
				song = songs[current_track];
				audio.src = `${url}${song.srcSong}`;
				audio.onloadeddata = function () {
					updateinfo();
				}
				$(".list-group-item").removeClass("song-active");
				$(this).addClass("song-active");
				guardarUltimaCancion(current_track);
			});
			
		}


		window.addEventListener("load", init(), false);
		function init() {
			audio = new Audio();
			song = songs[current_track];
			audio.src = `${url}${song.srcSong}`;
			coverWidget.src = `${url}${song.srcCover}`;
			cover.src = `${url}${song.srcCover}`;
			nombre_cancion.textContent = song.nombre;
			artista.textContent = `${song.artista.substring(0, 20)}...`;
			songActive(current_track);
			let volumen = localStorage.getItem("volumen") != null ? localStorage.getItem("volumen") : 0.25;
			$("#progressVolumen").children().css("width", `${volumen * 100}%`);

			audio.volume = volumen;
			document.title = song.nombre;
			let audioVolumen  = (localStorage.getItem("audioVolumen") != null ) ? localStorage.getItem("audioVolumen") :  false;
			if(audioVolumen =="false"){
				audio.muted = false;
				muted.innerHTML = `<i class="fas fa-volume-up"></i>`;
				let volumen = localStorage.getItem("volumen") != null ? localStorage.getItem("volumen") : 0.25;
				$("#progressVolumen").children().css("width", `${volumen * 100}%`);
			}
			if(audioVolumen =="true"){
				audio.muted = true;
				muted.innerHTML = `<i class="fas fa-volume-off"></i>`;
				$("#progressVolumen").children().css("width", `0%`);
			}
		}

		play.onclick = function () {
			playing ? audio.pause() : audio.play();
		}

		botonReproducir.onclick = function(){
			playing ? audio.pause() : audio.play();
		}


		muted.onclick = function (){
			if(!audio.muted){
				audio.muted  = true;
				muted.innerHTML = `<i class="fas fa-volume-off"></i>`;
				$("#progressVolumen").children().css("width", `0%`);
			}else{
				audio.muted = false;
				muted.innerHTML = `<i class="fas fa-volume-up"></i>`;
				let volumen = localStorage.getItem("volumen") != null ? localStorage.getItem("volumen") : 0.25;
				$("#progressVolumen").children().css("width", `${volumen * 100}%`);
			}
			localStorage.setItem("audioVolumen",audio.muted);
		}

		audio.addEventListener("pause", function () {
			play.innerHTML = `<i class="fas fa-play"></i>`;
			botonReproducir.innerHTML = "Reproducir";
			playing = false;

		}, false);

		audio.addEventListener("playing", function () {
			play.innerHTML = `<i class="fas fa-stop-circle"></i>`;
			botonReproducir.innerHTML = "Pausar";
			playing = true;
		}, false);

		audio.addEventListener("loadedmetadata", function () {
			duration = this.duration;
			tiempoTotal.innerHTML = milisegundoSegundoMinutos(duration);
		}, false);

		audio.addEventListener("ended", function () {
			nextTrack();
		}, false);

		audio.addEventListener("timeupdate", function () {
			currentTime = audio.currentTime;
			let por = Math.round((currentTime * 100) / duration);
			tiempoActual.innerHTML = milisegundoSegundoMinutos(currentTime);
			$("#progressCancion").children().css("width", `${por}%`);
		}, false)


		function milisegundoSegundoMinutos(milisegundos) {
			let minutos, segundos;
			minutos = Math.floor(milisegundos / 60);
			minutos = (minutos < 10) ? "0" + minutos : minutos;
			segundos = Math.floor(milisegundos % 60);
			segundos = (segundos < 10) ? "0" + segundos : segundos;
			return minutos + ":" + segundos;
		}

		next.addEventListener("click", nextTrack, false);
		function nextTrack() {
			current_track++;
			current_track = current_track % (songs.length);
			song = songs[current_track];

			audio.src = `${url}${song.srcSong}`;
			audio.onloadeddata = function () {
				updateinfo();
			}
			songActive(current_track);
			guardarUltimaCancion(current_track);

		}

		prev.addEventListener("click", prevTrack, false);
		function prevTrack() {
			current_track--;
			current_track = current_track == -1 ? songs.length - 1 : current_track;
			song = songs[current_track];

			audio.src = `${url}${song.srcSong}`;
			audio.onloadeddata = function () {
				updateinfo();
			}
			songActive(current_track);
			guardarUltimaCancion(current_track);

		}

		function guardarUltimaCancion(ultimaCancion) {
			localStorage.setItem("ultimaCancion", ultimaCancion);
		}

		function updateinfo() {
			cover.src = `${url}${song.srcCover}`;
			nombre_cancion.textContent = song.nombre;
			artista.textContent = song.artista;
			document.title = song.nombre;
			coverWidget.src = `${url}${song.srcCover}`;
			artista.textContent = `${song.artista.substring(0, 20)}...`;
			cover.onload = function () {
				audio.play();
			}
		}

		$("#progressCancion").click(function (e) {
			let anchoMaximo = $(this).width();
			let posicion = e.pageX - $(this).offset().left;
			let porcentaje = Math.round(posicion / anchoMaximo * 100);
			porcentaje = (porcentaje > 100) ? 100 : porcentaje;
			$(this).children().css("width", `${porcentaje}%`);
			audio.play();
			audio.currentTime = (porcentaje * duration) / 100;

		});

		$("#progressVolumen").click(function (e) {
			let anchoMaximo = $(this).width();
			let posicion = e.pageX - $(this).offset().left;
			let porcentaje = Math.round(posicion / anchoMaximo * 100);
			porcentaje = (porcentaje > 100) ? 100 : porcentaje;
			$(this).children().css("width", `${porcentaje}%`);
			audio.volume = porcentaje / 100;
			localStorage.setItem("volumen", porcentaje / 100);
			audio.muted= false;
			localStorage.setItem("audioVolumen",false);
		});
	})